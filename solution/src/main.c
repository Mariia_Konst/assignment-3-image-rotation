#include <stdio.h>
#include <stdlib.h>

#include "bmp.h"
#include "rotate.h"
#include "image.h"

#define AMOUNT 4

void printError(const char *string) {
    fprintf(stderr, "%s", string);
}

void badArgs(int err) {
    if (err == 0)
        printError("not enough data\n ./image-transformer <source-image> <transformed-image> <angle>\n");
    else if (err == 1)
        printError("couldn`t open 1st file");
    else if (err == 2)
        printError("couldn`t open 2nd file");
    else if (err == 11)
        printError("couldn`t read 1st file");
    else if (err == 22)
        printError("couldn`t write in 2nd file");
    else if (err == 3)
        printError("bad angle");
    exit(1);
}


int main(int argc, char **argv) {

    if (argc != AMOUNT) {
        badArgs(0);
    }

    int angle = atoi(argv[3]);

    struct image img;

    if (angle % 90 == 0 || angle < -270 || angle > 270) {
        from_bmp(argv[1], &img);
        rotate(&img, angle);
    } else {
        free(img.data);
        badArgs(3);
    }


    if (to_bmp(argv[2], &img) != WRITE_OK) {
        free(img.data);
        badArgs(22);

    }
    free(img.data);

    return 0;
}
