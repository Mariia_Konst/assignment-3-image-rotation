//
// Created by Мария on 03.02.2024.
//

#define BFTYPE 0x4D42
#define BITCOUNT 24
#define COMPRESSION 0
#define FOUR 4
#define RESERVED 0
#define HEADER_SIZE 40
#define COLORS_IMPORTANT 0
#define COLORS_USED 0
#define HEADER_PPM 2835
#define PLANES 1


#include <stdbool.h>
#include <malloc.h>

#include "bmp.h"
#include "main.h"

bool correct_header(const struct bmp_header *header) {
    if ((header->bfType != BFTYPE) || (header->biBitCount != BITCOUNT) || (header->biCompression != COMPRESSION))
        return false;
    return true;
}

enum read_status from_bmp(const char* fileName, struct image *img) {

    //open files
    FILE *file = fopen(fileName, "rb");
    if (!file) {
        badArgs(1);
    }

    struct bmp_header header;
    if (fread(&header, sizeof(struct bmp_header), 1, file) != 1) {
        return READ_INVALID_HEADER;
    }

    if (!correct_header(&header)) { return READ_INVALID_HEADER; }

    img->height = header.biHeight;
    img->width = header.biWidth;

    img->data = malloc(sizeof(struct pixel) * img->width * img->height);
    if (img->data == NULL) {
        free(img->data);
        return READ_NO_DATA;
    }

    struct pixel *curr = img->data;
    long padding = (FOUR - ((img->width * sizeof(struct pixel)) % FOUR)) % FOUR;

    for (int i = 0; i < img->height; ++i) {
        if (fread(curr, sizeof(struct pixel), img->width, file) != img->width) {
            free(img->data);
            fclose(file);
            return READ_STRANGE_DATA;
        }
        if (fseek(file, padding, SEEK_CUR) != 0) {
            free(img->data);
            fclose(file);
            return READ_ERROR;
        }
        curr += img->width;
    }
    fclose(file);
    return READ_OK;
}

enum write_status to_bmp(const char *fileName, struct image *img) {

    //open2 & write
    FILE *file = fopen(fileName, "wb");
    if (!file) {
        free(img->data);
        badArgs(2);
    }

    long padding = (FOUR - ((img->width * sizeof(struct pixel)) % FOUR)) % FOUR;
    struct bmp_header header = {
            .bfType = BFTYPE,
            .bfileSize = (img->width * sizeof(struct pixel) + padding) * img->height + sizeof(struct bmp_header),
            .bfReserved = RESERVED,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = HEADER_SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = PLANES,
            .biBitCount = BITCOUNT,
            .biCompression = COMPRESSION,
            .biSizeImage = (img->width * sizeof(struct pixel) + padding) * img->height,
            .biXPelsPerMeter = HEADER_PPM,
            .biYPelsPerMeter = HEADER_PPM,
            .biClrUsed = COLORS_USED,
            .biClrImportant = COLORS_IMPORTANT
    };

    if (!fwrite(&header, sizeof(struct bmp_header), 1, file)) {
        fclose(file);
        return WRITE_INVALID_HEADER;
    }

    for (int y = 0; y < img->height; y++) {
        if (fwrite(&img->data[img->width * y], sizeof(struct pixel), img->width, file) != img->width) {
            fclose(file);
            return WRITE_STRANGE_DATA;
        }
        if (fseek(file, padding, SEEK_CUR) != 0) {
            fclose(file);
            return WRITE_ERROR;
        }
    }
    fclose(file);
    return WRITE_OK;
}
