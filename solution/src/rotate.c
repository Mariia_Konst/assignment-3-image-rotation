//
// Created by Мария on 04.02.2024.
//

#include <stdint.h>
#include <stdlib.h>


#include "rotate.h"

void rotateImage90(struct image* img, struct pixel* rotated_data) {

    for (int i = 0; i < img->height; ++i) {
        for (int j = 0; j < img->width; ++j) {
            int index = i * img->width + j;
            int rotatedIndex = (img->width - 1 - j) * img->height + i;
            rotated_data[rotatedIndex] = img->data[index];
        }
    }

    for (int i = 0; i < img->width * img->height; ++i) {
        img->data[i] = rotated_data[i];
    }

}

void rotateImage270(struct image* img, struct pixel* rotated_data) {

    for (int i = 0; i < img->height; ++i) {
        for (int j = 0; j < img->width; ++j) {
            int index = i * img->width + j;
            int rotatedIndex = j * img->height + (img->height - 1 - i);
            rotated_data[rotatedIndex] = img->data[index];
        }
    }

    for (int i = 0; i < img->width * img->height; ++i) {
        img->data[i] = rotated_data[i];
    }

}

void rotateImage180(struct image* img, struct pixel* rotated_data) {

    for (int i = 0; i < img->height; ++i) {
        for (int j = 0; j < img->width; ++j) {
            int index = i * img->width + j;
            int rotatedIndex = (img->height - 1 - i) * img->width + (img->width - 1 - j);
            rotated_data[rotatedIndex] = img->data[index];
        }
    }

    for (int i = 0; i < img->width * img->height; ++i) {
        img->data[i] = rotated_data[i];
    }

}

void rotate(struct image* img, int angle) {

    struct pixel* rotated_data = malloc(img->width * img->height * sizeof(struct pixel));
    if (rotated_data == NULL) {
        exit(1);
    }

    if (angle == 90 || angle == -270) {

        rotateImage90(img,rotated_data);

        uint64_t temp = img->width;
        img->width = img->height;
        img->height = temp;
    }
    else if (angle == -90 || angle == 270) {

        rotateImage270(img,rotated_data);

        uint64_t temp = img->width;
        img->width = img->height;
        img->height = temp;
    }
    else if (angle == 180 || angle == -180) {
        rotateImage180(img,rotated_data);
    }

    free(rotated_data);

}
