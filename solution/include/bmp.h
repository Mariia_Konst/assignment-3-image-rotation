#ifndef ASSIGNMENT_3_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_3_IMAGE_ROTATION_BMP_H

#include  <stdint.h>
#include <stdio.h>

#include "image.h"

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)

enum read_status  {
    READ_OK = 0,
    READ_INVALID_HEADER,
    READ_NO_DATA,
    READ_STRANGE_DATA,
    READ_ERROR
};

enum write_status  {
    WRITE_OK = 0,
    WRITE_INVALID_HEADER,
    WRITE_STRANGE_DATA,
    WRITE_ERROR
};

enum read_status from_bmp(const char* fileName, struct image* img);

enum write_status to_bmp(const char* fileName, struct image* img);
#endif
