//
// Created by Мария on 04.02.2024.
//

#ifndef ASSIGNMENT_3_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_3_IMAGE_ROTATION_ROTATE_H

#include "image.h"

void rotate(struct image* img, int angle);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_ROTATE_H
